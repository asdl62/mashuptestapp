const lastfm_api_url="https://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=2b42b158fd5296f97a0b8573be169ab6&format=json";
const eventPerfSearch_url="https://api.eventful.com/json/performers/search?app_key=9RX9mPk7XN2K99wh&scheme=https&keywords=";
const eventPerfget_url="https://api.eventful.com/json/performers/get?app_key=9RX9mPk7XN2K99wh&scheme=https&show_events=true&id=";
const eventGet_url=" https://api.eventful.com/json/events/get?app_key=9RX9mPk7XN2K99wh&scheme=https&id=";
const reise_url="https://www.reisewarnung.net/api?country=";

async function topTracks(){
    
    const response = await fetch(lastfm_api_url);
    const data = await response.json();

    varName1 =data.tracks.track[0].artist.name;
    varName2 =data.tracks.track[1].artist.name;
    varName3 =data.tracks.track[2].artist.name;
    varName4 =data.tracks.track[3].artist.name;
    varName5 =data.tracks.track[4].artist.name;

    var1 ="Platz 1: " +  varName1 + " - " + data.tracks.track[0].name;
    var2 ="Platz 2: " +  varName2 + " - " + data.tracks.track[1].name;
    var3 ="Platz 3: " +  varName3 + " - " + data.tracks.track[2].name;
    var4 ="Platz 4: " +  varName4 + " - " + data.tracks.track[3].name;
    var5 ="Platz 5: " +  varName5 + " - " + data.tracks.track[4].name;


   
    document.getElementById("art1").textContent = var1;
    document.getElementById("art2").textContent = var2;
    document.getElementById("art3").textContent = var3;
    document.getElementById("art4").textContent = var4;
    document.getElementById("art5").textContent = var5;

    /*
    console.log(data.tracks.track[0].artist.name + " - " + data.tracks.track[0].name);
    console.log(data.tracks.track[1].artist.name + " - " + data.tracks.track[1].name);
    console.log(data.tracks.track[2].artist.name + " - " + data.tracks.track[2].name);
    console.log(data.tracks.track[3].artist.name + " - " + data.tracks.track[3].name);
    console.log(data.tracks.track[4].artist.name + " - " + data.tracks.track[4].name);
*/
}

async function perfSearch(){
    var eventPerfName;
    var e = document.getElementById("sel1");
    var value = e.options[e.selectedIndex].value;
    
    if(value == 0){

    }else if(value == 1){
        eventPerfName = varName1;
    }else if(value == 2){
        eventPerfName = varName2;
    }else if(value == 3){
        eventPerfName = varName3;
    }else if(value == 4){
        eventPerfName = varName4;
    }else if(value == 5){
        eventPerfName = varName5;
    }

    var replacedName = eventPerfName.split(" ").join("-");

    document.getElementById("kunstler").textContent = eventPerfName;
    
    
    
    const response = await fetch(eventPerfSearch_url+replacedName);
    const perfIdData = await response.json();

    var perfId = perfIdData.performers.performer.id;
    
    perfGet(perfId);

    
    
}


async function perfGet(perfId){
    const response = await fetch(eventPerfget_url+perfId);
    const perfGetData = await response.json();

    
    console.log(perfGetData);
    
    
    
    
    if(perfGetData.event_count == 0){
        document.getElementById("datum").textContent = "-";
        document.getElementById("ort").textContent = "-";
        document.getElementById("warnung").textContent = "-";
        document.getElementById("advice").textContent = "Derzeit sind keine Konzerte geplant";
    }else{
        var eventId = perfGetData.events.event[0].id;
          eventGet(eventId);
    }
    
  
    
}

async function eventGet(eventId){

    const response = await fetch(eventGet_url+eventId);
    const eventGetData = await response.json();

    var startTime=eventGetData.start_time;
    var city=eventGetData.city+" / "+eventGetData.country
    var countryIso=eventGetData.country_abbr2;

    document.getElementById("datum").textContent = startTime;
    document.getElementById("ort").textContent = city;
    

    getReise(countryIso);

}

async function getReise(countryIso){
    const response = await fetch(reise_url+countryIso);
    const reiseData = await response.json();

    var situation = reiseData.data.situation.rating;
    var advice = reiseData.data.lang.de.advice;
    var url = reiseData.data.lang.de.url_details;

    document.getElementById("warnung").textContent = situation;
    document.getElementById("advice").textContent = advice;
 
}
